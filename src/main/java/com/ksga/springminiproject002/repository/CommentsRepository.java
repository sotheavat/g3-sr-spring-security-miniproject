package com.ksga.springminiproject002.repository;

import com.ksga.springminiproject002.model.Comments;
import com.ksga.springminiproject002.model.Replies;
import com.ksga.springminiproject002.payload.request.RepliesRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.Mapping;

import java.util.List;

@Mapper
public interface CommentsRepository {

    @Insert("insert into comments (user_id, post_id,content,created_at,update_at) values (#{userId}, #{postId},#{content},#{createdAt},#{updateAt})")
    void createComment (Comments comments);


    @Insert("insert into comments (user_id, post_id,content,created_at,update_at, parent_id) values (#{userId}, #{postId},#{content},#{createdAt},#{updateAt}, #{parentId}) ")
    String createReplies (Comments comments);

    @Select("SELECT * FROM comments where post_id =#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "content", column = "fullname"),
            @Result(property = "userId", column = "username"),
            @Result(property = "parentId", column = "password"),
            @Result(property = "postId", column = "profile"),
            @Result(property = "createdAt", column = "is_closed"),
            @Result(property = "updateAt", column = "created_at"),
            @Result(property = "reply", column = "id", many = @Many(select = "findAllRepliesByCommentId"))
    })
    List<Comments> getAllComment(int id);

    @Select("SELECT * FROM replies WHERE comment_id = #{id}")
    public List<Replies> findAllRepliesByCommentId(@Param("id") int id);

    @Select("select * from comments where parent_id = #{id}")
    Comments getAllReply(int id);

    @Select("SELECT * FROM replies WHERE comment_id = #{commentId}")
    List<Replies> getRepliedComment(RepliesRequest reply);

    @Select("SELECT * FROM replies WHERE comment_id = #{id}")
    List<Replies> getRepliedByCommentId(int id);

    @Insert("INSERT INTO replies(comment_id,content) VALUES(#{commentId},#{content})")
    void addReply(Replies reply);
}
