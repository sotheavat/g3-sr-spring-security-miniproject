package com.ksga.springminiproject002.repository.provider;

import com.ksga.springminiproject002.model.post.Post;
import com.ksga.springminiproject002.model.post.PostFilter;
import com.ksga.springminiproject002.payload.request.PostRequest;
import com.ksga.springminiproject002.utilities.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class PostProvider {
    public String findAll(@Param("paging") Paging paging){

        return new SQL(){{
            SELECT("*");
            FROM("posts");
            INNER_JOIN("users on posts.user_id = users.id");
            ORDER_BY("posts.id desc  limit #{paging.limit} offset #{paging.offset}");
        }}.toString();
    }

    public String insert(Post post){
        return new SQL(){{
            INSERT_INTO("posts");
            VALUES("caption,image,user_id,created_at,update_at","#{caption},#{image},#{user_id},#{createAt}, #{updateAt}");
        }}.toString();
    }

    public String countAll(){
        return new SQL(){{
            SELECT("count(id)");
            FROM("posts");
        }}.toString();
    }

    public String findOne(@Param("id") int id){
        return new SQL(){{
            SELECT("*");
            FROM("posts");
            INNER_JOIN("users on users.id = posts.id");
            WHERE("posts.id = #{id}");
        }}.toString();
    }

    public String update(int id, PostRequest post){
        return new SQL(){{
            UPDATE("posts a");
            SET("caption=#{post.caption},image=#{post.image}, update_at=#{post.update_at}");
            WHERE("a.id=#{id}");
        }}.toString();
    }

    public String delete(int id){
        return new SQL(){{
            DELETE_FROM("posts");
            WHERE("posts.id=#{id}");
        }}.toString();
    }

    public String filterPost(@Param("filter") PostFilter filter){
        return new SQL(){{
            SELECT("*");
            FROM("posts");
            INNER_JOIN("users on posts.user_id = users.id");
            if(filter.getCaption()!=null){
                WHERE("posts.caption ILIKE '%' || #{filter.caption} || '%' ");
            }
            if(filter.getUsername()!=null){
                WHERE("users.username = #{filter.username}");
            }
            if(filter.getUser_id() == filter.getUser_id()){
                WHERE("posts.user_id = #{filter.user_id}");
            }
            ORDER_BY("posts.id DESC ");
        }}.toString();
    }

    public String findById(int id){
        return new SQL(){{
            SELECT("*");
            FROM("posts");
            INNER_JOIN("users on posts.user_id = users.id");
            WHERE("posts.user_id=#{id}");
        }}.toString();
    }
}
