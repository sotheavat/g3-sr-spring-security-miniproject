package com.ksga.springminiproject002.repository;

import com.ksga.springminiproject002.model.post.Post;
import com.ksga.springminiproject002.model.post.PostFilter;
import com.ksga.springminiproject002.payload.dto.PostDto;
import com.ksga.springminiproject002.payload.request.PostRequest;
import com.ksga.springminiproject002.repository.provider.PostProvider;
import com.ksga.springminiproject002.utilities.Paging;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository {
    @SelectProvider(type = PostProvider.class,method = "findAll")
    public List<Post> findAll(@Param("paging") Paging paging);

    @SelectProvider(type = PostProvider.class,method = "countAll")
    public int countAllRecord();
    public Post findLastArticle();

    @InsertProvider(type = PostProvider.class,method = "insert")
    public boolean insert(Post post);

    @UpdateProvider(type = PostProvider.class,method = "update")
    public boolean update(@Param("id") int id, PostRequest post);

    @DeleteProvider(type = PostProvider.class,method = "delete")
    public boolean delete(@Param("id") int id);

    @SelectProvider(type = PostProvider.class,method = "findOne")
    public Post findOne(@Param("id") int id);

    @SelectProvider(type = PostProvider.class,method = "filterPost")
    public List<Post> filterPost(@Param("filter") PostFilter filter);

    @Select("SELECT * FROM posts inner join users u on posts.user_id = u.id WHERE posts.id = (SELECT MAX(posts.id) FROM posts)")
    public Post findLastPost();
}
