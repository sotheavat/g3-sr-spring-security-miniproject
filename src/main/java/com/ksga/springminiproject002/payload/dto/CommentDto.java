package com.ksga.springminiproject002.payload.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class CommentDto {
    private int id;
    private String content;
    private String replies;
    private int postId;
    private UserDto user;
    private int parentId;
}
