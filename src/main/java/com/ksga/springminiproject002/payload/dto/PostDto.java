package com.ksga.springminiproject002.payload.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDto {
    private int id;
    private String caption;
    private String image;
    private int likes;
    private boolean owner;
    private int user_id;
    private String username;
//    private Date create_at = new Date();
//    private Date update_at = new Date();
}
