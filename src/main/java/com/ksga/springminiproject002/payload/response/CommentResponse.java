package com.ksga.springminiproject002.payload.response;

import com.ksga.springminiproject002.payload.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentResponse {
    private String content;
    private int parentId;
    private int postId;
    private UserDto user;
}
