package com.ksga.springminiproject002.controller.rest;

import com.ksga.springminiproject002.model.ERole;
import com.ksga.springminiproject002.model.Role;
import com.ksga.springminiproject002.model.User;
import com.ksga.springminiproject002.payload.ResponsePayload;
import com.ksga.springminiproject002.payload.request.LoginRequest;
import com.ksga.springminiproject002.payload.request.RegisterRequest;
import com.ksga.springminiproject002.payload.response.LoginResponse;
import com.ksga.springminiproject002.payload.response.RegisterResponse;
import com.ksga.springminiproject002.repository.UserRepository;
import com.ksga.springminiproject002.repository.UserRoleRepository;
import com.ksga.springminiproject002.security.jwt.JwtUtils;
import com.ksga.springminiproject002.security.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthRestController {

    AuthenticationManager authenticationManager;
    JwtUtils jwtUtils;
    UserRepository userRepository;
    PasswordEncoder encoder;
    @Autowired
    UserRoleRepository userRoleRepository;
    @Autowired
    public AuthRestController(AuthenticationManager authenticationManager, UserRepository userRepository, PasswordEncoder encoder, JwtUtils jwtUtils) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/register")
    public ResponseEntity<Object> registerUser(@RequestBody RegisterRequest registerRequest) {
        try {

            if (userRepository.findUserByUsername(registerRequest.getUsername()).isPresent()) {
                return new ResponseEntity(new ResponsePayload(HttpStatus.BAD_REQUEST.name(),"Username is already taken!",HttpStatus.BAD_REQUEST.value(),new RegisterResponse(0L,"","","","",new HashSet<>())),HttpStatus.BAD_REQUEST);
            }

            User user = new User(
                    registerRequest.getUsername(),
                    encoder.encode(registerRequest.getPassword())
            );
            Set<String> strRoles = registerRequest.getRoles();
            Set<Role> roles = new HashSet<>();
            if (strRoles == null) {
                Role userRole = new Role();;
                userRole.setName(ERole.ROLE_USER);
                roles.add(userRole);
                System.out.println("i was here 3");
            } else {
                strRoles.forEach(role -> {
                    if ("ROLE_ADMIN".equals(role)) {
                        Role adminRole = new Role();;
                        adminRole.setName(ERole.ROLE_ADMIN);
                        adminRole.setId(1L);
                        roles.add(adminRole);
                    } else {
                        Role userRole = new Role();
                        userRole.setName(ERole.ROLE_USER);
                        userRole.setId(2L);
                        roles.add(userRole);

                    }
                });
            }

            user.setRoles(roles);
            user.setFullName(registerRequest.getFullName());
            userRepository.save(user);
            System.out.println(user);
            user = userRepository.findUserByUsername(user.getUsername()).get();
            User finalUser = user;
            roles.forEach(role -> {
                System.out.println("this run many time"+ finalUser.getId().intValue()+"role id"+role.getId().intValue());
                userRoleRepository.save(finalUser.getId().intValue(),role.getId().intValue());
            });
            return new ResponseEntity(new ResponsePayload(HttpStatus.OK.name(),"",HttpStatus.OK.value(),new RegisterResponse(finalUser.getId(), user.getFullName(), user.getUsername(), user.getPassword(),"",roles.stream().map(element ->  element.getName().name()).collect(Collectors.toSet()))),HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity(new ResponsePayload(HttpStatus.BAD_REQUEST.name(), ex.getMessage(), HttpStatus.BAD_REQUEST.value(),new RegisterResponse(0L,"","","","",new HashSet<>())),HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<Object> authenticateUser(@RequestBody LoginRequest loginRequest) {
        try{
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            Set<String> roles = userDetails.getAuthorities()
                    .stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toSet());
            return new ResponseEntity<>(new ResponsePayload(HttpStatus.OK.name(),"",HttpStatus.OK.value(),new LoginResponse(userDetails.getId(), jwt, userDetails.getUsername(), roles)), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ResponsePayload(HttpStatus.BAD_REQUEST.name(),e.getMessage(),HttpStatus.BAD_REQUEST.value(),new LoginResponse(0L, "", "", new HashSet<>())), HttpStatus.BAD_REQUEST);
        }
    }
}
