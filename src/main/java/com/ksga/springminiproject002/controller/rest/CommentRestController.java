package com.ksga.springminiproject002.controller.rest;

import com.ksga.springminiproject002.model.Comments;
import com.ksga.springminiproject002.model.Replies;
import com.ksga.springminiproject002.model.User;
import com.ksga.springminiproject002.payload.ResponsePayload;
import com.ksga.springminiproject002.payload.dto.UserDto;
import com.ksga.springminiproject002.payload.request.CommentRequest;
import com.ksga.springminiproject002.payload.request.RepliesRequest;
import com.ksga.springminiproject002.payload.response.CommentResponse;
import com.ksga.springminiproject002.payload.response.LoginResponse;
import com.ksga.springminiproject002.security.jwt.JwtUtils;
import com.ksga.springminiproject002.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api")
public class CommentRestController {



    private final CommentService commentService;
    private final JwtUtils jwtUtils;

    @Autowired
    public CommentRestController(CommentService commentService, JwtUtils jwtUtils) {
        this.commentService = commentService;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/comments")
    public ResponseEntity<ResponsePayload> createComment (@RequestBody CommentRequest commentRequest, @RequestHeader HttpHeaders header) {
        Comments comments =  new Comments();
        comments.setContent(commentRequest.getContent());
        comments.setPostId(commentRequest.getPostId());
        User user = jwtUtils.getTokenFromHeader(header);
        comments.setUserId(user.getId().intValue());
        commentService.createComment(comments);
        UserDto userDto = new UserDto(user);
        CommentResponse commentResponse = new CommentResponse(comments.getContent(),comments.getParentId(),comments.getPostId(),userDto);
        return new ResponseEntity<>(new ResponsePayload(HttpStatus.OK.name(),"",HttpStatus.OK.value(),commentResponse), HttpStatus.CREATED);
    }
    @GetMapping("/comments/{id}/replies")
    public ResponseEntity<ResponsePayload> findRepliesByCommentId(@PathVariable int id) {
        List<Replies> replies = commentService.getRepliedByCommentId(id);
        return new ResponseEntity<>(new ResponsePayload(HttpStatus.OK.name(),"",HttpStatus.OK.value(),replies), HttpStatus.CREATED);
    }
    @GetMapping("/posts/{id}/comments")
    public ResponseEntity<ResponsePayload> findCommentByPostId (@PathVariable int id) {
        List<Comments> comments = commentService.getCommentByPostId(id);
        return new ResponseEntity<>(new ResponsePayload(HttpStatus.OK.name(),"",HttpStatus.OK.value(),comments), HttpStatus.CREATED);
    }
    @PostMapping("/replies")
    public ResponseEntity<ResponsePayload> addReplies (@RequestBody RepliesRequest repliesRequest) {
        List<Comments> comments = commentService.getCommentByPostId(repliesRequest.getPostId());
        Comments comment =  new Comments();
        for (int i = 0; i < comments.size(); i++) {
            if(comments.get(i).getId() == repliesRequest.getParentId()){
                comment = comments.get(i);
            }
        }
        if (comment==null){
            return new ResponseEntity<>(new ResponsePayload(HttpStatus.BAD_REQUEST.name(),"Comment not Found",HttpStatus.BAD_REQUEST.value(),new Comments()), HttpStatus.BAD_REQUEST);
        }
        Replies reply =  new Replies(0, repliesRequest.getContent(), comment.getId());
        commentService.addReply(reply);
        return new ResponseEntity<>(new ResponsePayload(HttpStatus.OK.name(),"",HttpStatus.OK.value(),reply), HttpStatus.CREATED);
    }
}
