package com.ksga.springminiproject002.controller.rest;

import com.ksga.springminiproject002.model.post.Post;
import com.ksga.springminiproject002.model.post.PostFilter;
import com.ksga.springminiproject002.payload.dto.PostDto;
import com.ksga.springminiproject002.payload.mapper.PostMapper;
import com.ksga.springminiproject002.payload.request.PostRequest;
import com.ksga.springminiproject002.service.PostService;
import com.ksga.springminiproject002.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/posts")
public class PostRestController {
    private final PostService postService;

    @Autowired
    private PostMapper postMapper;

    @Autowired
    public PostRestController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public ResponseEntity<Map<String, Object>> getAllPosts (@RequestParam int page, @RequestParam int limit) {
        Post post = new Post();
        Paging paging = new Paging();
        paging.setPage(page);
        paging.setLimit(limit);
        Map<String, Object> response = new HashMap<>();
        response.put("data",postService.findAll(paging));
        response.put("pagination",paging);
        response.put("message","successfully fetched");
        response.put("status", HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}/view")
    public PostDto getPostById(@PathVariable int id) {
        return postService.findOne(id);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Map<String, Object>> insertPost(@RequestBody PostRequest post){
        Map<String, Object> response = new HashMap<>();
        response.put("message","successfully create");
        response.put("data",postService.insert(post));
        response.put("status", HttpStatus.CREATED);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/filter")
    public ResponseEntity<Map<String, Object>> filterPost(@RequestParam(value = "caption", required=false) String caption, @RequestParam(value = "limit", required=false) int limit, @RequestParam(value = "page", required=false) int page, @RequestParam(value = "userId", required=false) int user_id, @RequestParam(value = "username", required=false) String username){
        PostFilter filter = new PostFilter();
        Paging paging = new Paging();
        filter.setCaption(caption);
        filter.setUsername(username);
        filter.setUser_id(user_id);
        paging.setLimit(limit);
        paging.setPage(page);
        Map<String, Object> response = new HashMap<>();
        response.put("data",postService.filterPost(filter));
        response.put("limit",paging.getLimit());
        response.put("page",paging.getPage());
        response.put("message","successfully filtered");
        response.put("status", HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @PatchMapping("/react")
    public ResponseEntity<Map<String, Object>> reactArticle(@RequestBody PostRequest post){
        Map<String, Object> response = new HashMap<>();

        response.put("message","successfully create");
        response.put("data",postService.insert(post));
        response.put("status", HttpStatus.CREATED);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}/update")
    public Post update(@PathVariable int id, @RequestBody PostRequest post) {
        return postService.update(id, post);
    }

    @DeleteMapping("/{id}/delete")
    public String delete(@PathVariable int id)
    {
        postService.delete(id);
        return "delete success";
    }
}
