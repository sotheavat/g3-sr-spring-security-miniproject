package com.ksga.springminiproject002.controller.rest;

import com.ksga.springminiproject002.model.FileUpload;
import com.ksga.springminiproject002.payload.ResponsePayload;
import com.ksga.springminiproject002.service.impl.FileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1/file")
public class FileRestController {
    private final FileServiceImpl filesService;
    @Autowired
    public FileRestController(FileServiceImpl filesService) {
        this.filesService = filesService;
    }

    @PostMapping(value = "/addFile",consumes = "multipart/form-data")
    public ResponseEntity<ResponsePayload> addFile(@RequestPart MultipartFile file){
        String fileName;
        try {
            fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        }catch(NullPointerException e){
            return new ResponseEntity<>(new ResponsePayload(HttpStatus.BAD_REQUEST.name(),e.getMessage(),HttpStatus.BAD_REQUEST.value(), ""), HttpStatus.BAD_REQUEST);
        }

        Path path = Paths.get("src/main/resources/files/"+fileName);
        while (true){
            try {
                Files.copy(file.getInputStream(),path, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException n){
                if (new File("src/main/resources/files/").mkdirs()){
                    continue;
                }
            }
            break;
        }
        filesService.save(new FileUpload(fileName));
        return new ResponseEntity<>(new ResponsePayload(HttpStatus.CREATED.name(),"",HttpStatus.CREATED.value(),"/file/"+fileName), HttpStatus.CREATED);
    }
    @GetMapping("/getFile")
    public ResponseEntity<ResponsePayload> getFile(@RequestParam("id") int id){
        try {
            FileUpload file = filesService.get(id);
            if (file == null){
              throw new IllegalArgumentException("File Not Found!");
            }
            return new ResponseEntity<>(new ResponsePayload(HttpStatus.OK.name(),"",HttpStatus.OK.value(),file), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(new ResponsePayload(HttpStatus.NOT_FOUND.name(),ex.getMessage(),HttpStatus.NOT_FOUND.value(),new FileUpload()), HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/getAllFile")
    public ResponseEntity<ResponsePayload>getAllFile(){
        try {
            List<FileUpload> files = filesService.findAll();
            return new ResponseEntity<>(new ResponsePayload(HttpStatus.OK.name(),"",HttpStatus.OK.value(),files), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(new ResponsePayload(HttpStatus.BAD_REQUEST.name(),ex.getMessage(),HttpStatus.BAD_REQUEST.value(),new ArrayList<FileUpload>()), HttpStatus.BAD_REQUEST);
        }
    }

}
