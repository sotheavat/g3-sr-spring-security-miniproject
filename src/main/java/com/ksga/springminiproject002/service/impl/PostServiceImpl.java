package com.ksga.springminiproject002.service.impl;

import com.ksga.springminiproject002.model.post.Post;
import com.ksga.springminiproject002.model.post.PostFilter;
import com.ksga.springminiproject002.payload.dto.PostDto;
import com.ksga.springminiproject002.payload.mapper.PostMapper;
import com.ksga.springminiproject002.payload.request.PostRequest;
import com.ksga.springminiproject002.repository.PostRepository;
import com.ksga.springminiproject002.service.PostService;
import com.ksga.springminiproject002.utilities.Paging;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    private PostRepository repository;
    private PostMapper postMapper;
    ModelMapper mapper = new ModelMapper();
    @Autowired
    public PostServiceImpl(PostRepository repository,
                             PostMapper postMapper) {
        this.repository = repository;
        this.postMapper = postMapper;
    }

    @Override
    public List<PostDto> findAll(Paging paging) {
        paging.setTotalCount(repository.countAllRecord());
        List<PostDto> dtoList = new ArrayList<>();
        for(Post a: repository.findAll(paging)){
            PostDto dto = mapper.map(a,PostDto.class);
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public PostDto insert(PostRequest post) {
        Post a = postMapper.mapRequestToModel(post);
        if(repository.insert(a)){
            PostDto dto = postMapper.mapToDto(repository.findLastPost());
            return dto;
        }
        else
            return null;
    }

    @Override
    public PostDto findOne(int id) {
        PostDto dto = mapper.map(repository.findOne(id),PostDto.class);
        return dto;
    }

    @Override
    public Post update(int id, PostRequest post) {
        if(repository.update(id, post)){
            return repository.findOne(id);
        }
        else
            return null;
    }

    @Override
    public boolean delete(int id) {
        return repository.delete(id);
    }

    @Override
    public List<Post> filterPost(PostFilter filter) {
        return repository.filterPost(filter);
    }
}
