package com.ksga.springminiproject002.service;

import com.ksga.springminiproject002.model.Comments;
import com.ksga.springminiproject002.model.Replies;
import com.ksga.springminiproject002.payload.request.RepliesRequest;
import org.springframework.stereotype.Service;

import java.util.List;


public interface CommentService {
    void createComment (Comments comments);
    void createReplies (Comments comments);

    List<Comments> getCommentByPostId(int id);
    Comments getRepliesByParentId(int id);

    List<Replies> getRepliedComment(RepliesRequest request);

    List<Replies> getRepliedByCommentId(int id);

    void addReply(Replies reply);
}
