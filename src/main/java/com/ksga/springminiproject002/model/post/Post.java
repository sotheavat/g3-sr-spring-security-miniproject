package com.ksga.springminiproject002.model.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Post {
    private int id;
    private String caption;
    private String image;
    private int likes;
    private boolean owner;
    private int user_id;
    private String username;
    private Date createAt = new Date();
    private Date updateAt = new Date();
}
