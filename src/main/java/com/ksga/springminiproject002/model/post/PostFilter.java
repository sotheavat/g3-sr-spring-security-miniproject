package com.ksga.springminiproject002.model.post;

import com.ksga.springminiproject002.model.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostFilter {
    private String caption;
    private int user_id;
    private String username;
    private String password;
    private Set<Role> roles = new HashSet<>();
    private String imageUrl;
    private Boolean isClosed = false;
    private Date createAt = new Date();
    private Date updateAt = new Date();
}
